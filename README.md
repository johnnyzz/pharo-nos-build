Building PharoNOS
=================
Build status · [![build status](https://gitlab.com/johnnyzz/pharo-nos-build/badges/master/build.svg)](https://gitlab.com/johnnyzz/pharo-nos-build/commits/master)
```sh
 git clone git@gitlab.com:johnnyzz/pharo-vm.git pharo-vm/
 cd pharo-vm
 git submodule init
 git submodule update --recursive --remote
 cd ..
 mkdir pharo-nos-build
 cd pharo-nos-build
 cmake ../pharo-vm/ -DST_SCRIPT=gen-pharo-nos-stack -DST_PLATFORM_NAME=PharoNOS
 make
 ```